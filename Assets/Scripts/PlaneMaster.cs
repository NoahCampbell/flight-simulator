using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneMaster : MonoBehaviour
{
    [Header("Movement")]
    [System.NonSerialized] public float speed = 3.4f;
    [System.NonSerialized] public float turnSpeed = 1.5f;
    [System.NonSerialized] public float rotationSpeed = 1f;
    [System.NonSerialized] public float maxSpeed = 100f;
    [System.NonSerialized] public float takeOffSpeed = 30f;
    [System.NonSerialized] public float landingSpeed = 10f;
    [System.NonSerialized] public float rotorSpeed = 50f;
    [System.NonSerialized] public float gravity = 0.999f;
    [System.NonSerialized] public float yGravity = 0.01f;
    [System.NonSerialized] public float angularGravity = 0.99f;
    [System.NonSerialized] public bool isGrounded = false;
}
