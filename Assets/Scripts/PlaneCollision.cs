using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneCollision : MonoBehaviour
{
    public PlaneMaster planeMaster;

    // Start is called before the first frame update
    void Start()
    {
        planeMaster = GetComponent<PlaneMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            planeMaster.isGrounded = true;
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            planeMaster.isGrounded = false;
        }
    }
}
