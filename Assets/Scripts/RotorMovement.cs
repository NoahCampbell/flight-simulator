using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotorMovement : MonoBehaviour
{
    public Vector3 globalPosition;
    public GameObject player;
    public PlaneMovement planeMovement;
    public PlaneMaster planeMaster;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        globalPosition = player.transform.position;
        planeMovement = player.GetComponent<PlaneMovement>();
        planeMaster = player.GetComponent<PlaneMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        if (planeMovement.currentSpeed > 0)
        {
            FindRotorSpeed();
            transform.RotateAround(transform.position, transform.forward, planeMaster.rotorSpeed);            
        }
    }

    public void SetRotorSpeed(float speed)
    {
        planeMaster.rotorSpeed = speed;
    }


    public void FindRotorSpeed()
    {
        planeMaster.rotorSpeed = planeMovement.currentSpeed * 0.7f;
    }
}
