using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneMovement : MonoBehaviour
{
    public Rigidbody rb;
    public PlaneMaster planeMaster;
    public float currentSpeed;
    public float currentRotationSpeed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        planeMaster = GetComponent<PlaneMaster>();
        currentSpeed = 0f;
        currentRotationSpeed = 0f;

    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        ApplyGravity();

        if (rb.velocity.magnitude > 0)
        {
            CheckSpeed();
        }
    }

    public void Movement()
    {
        currentSpeed = rb.velocity.magnitude;
        currentRotationSpeed = rb.angularVelocity.magnitude;

        if (currentSpeed < planeMaster.maxSpeed)
        {
            if (Input.GetKey(KeyCode.W))
            {
                rb.AddForce(transform.forward * planeMaster.speed);
            }
        }
        if (currentSpeed > -planeMaster.maxSpeed)
        {
            if (Input.GetKey(KeyCode.S))
            {
                rb.AddForce(-transform.forward * planeMaster.speed);
            }
        }

        if (!planeMaster.isGrounded || Input.GetKey(KeyCode.W))
        {
            if (Input.GetKey(KeyCode.A))
            {
                rb.AddTorque(Vector3.down * planeMaster.turnSpeed);
            }
            if (Input.GetKey(KeyCode.D))
            {
                rb.AddTorque(Vector3.up * planeMaster.turnSpeed);
            }
        }

        if (!planeMaster.isGrounded)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb.AddRelativeTorque(Vector3.forward * planeMaster.rotationSpeed);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb.AddRelativeTorque(Vector3.back * planeMaster.rotationSpeed);
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                rb.AddRelativeTorque(Vector3.right * planeMaster.rotationSpeed);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                rb.AddRelativeTorque(Vector3.left * planeMaster.rotationSpeed);
            }
        }
    }

    public void CheckSpeed()
    {
        currentSpeed = rb.velocity.magnitude;
        currentRotationSpeed = rb.angularVelocity.magnitude;

        if (currentSpeed > 0 && currentSpeed < 0.0001f)
        {
            rb.velocity = Vector3.zero;
        }

        if (currentRotationSpeed > 0 && currentRotationSpeed < 0.0001f)
        {
            rb.angularVelocity = Vector3.zero;
        }
    }   

    public void ApplyGravity()
    {
        if (!planeMaster.isGrounded && currentSpeed < planeMaster.maxSpeed)
        {
            rb.velocity = new Vector3(rb.velocity.x * planeMaster.gravity, rb.velocity.y - planeMaster.yGravity, 
                                      rb.velocity.z * planeMaster.gravity);
        }
        else
        {
            rb.velocity = rb.velocity * planeMaster.gravity;            
        }
        rb.angularVelocity = rb.angularVelocity * planeMaster.angularGravity;
    }   
}
